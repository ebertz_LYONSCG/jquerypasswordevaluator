Jquery Password Evaluator


This jQuery plugin creates a visual progress bar that rates the strength of a password when attached to a text input field.
The plugin has a set of built in rules and evaluation functions to rate strength, and is highly customizable so users can
create their own rating functions. When requirements are not met, it displays customizable error messages below the input.
The bar displays as red when requirements are not met, yellow when minimum requirements are met, and scales to
bright green when maximum strength criteria are met.

How to use it

	Include passEval.js in your html file.
	Call $(<your-password-input-field>).passEval();
	Optional parameters : {
		rules,
		bonuses,
		evaluators,
		messages
		}


How it works

	Validators
		Validator functions return a boolean value based on the text input.
	
	Evaluators
		Evaluator functions take two parameters:
			Weight : The maximum progress bar value for this function
			Required : The minimum return value that is required (as a rule). 
		Evaluators return an integer based on text input between 0 and Weight.
	
	Rules vs Bonuses
		Rules
			Rules are password requirements. If a rule evaluates to false, an error message will be displayed, and the progress bar will turn red.
		Bonus
			Bonus functions are only evaluated if all rules are satisfied, and will increase the bar, but not give any errors if not met.
		Evaluators use a hybrid composition, where it is treated as a rule until the 'required' param is satisfied, and any additional value is treated as bonus