$.fn.passEval = function(options) {
	let defaults = {
		rules : {
			requireSpecial : true,
			requireUpper : true,
			requireLower : true,
			requireNumeric : true,
		},
		bonuses : {
		},
		evaluators : {
			evaluateLength : {
				weight : 10,
				required : 1,
				min : 6,
				max : 25,
			},
		},
		evalSettings : {
			weight : 1,
			required : 1
		}
	};
	let settings = {};
	settings.rules = pruneRules(Object.assign({}, defaults.rules, options ? options.rules : {}));
	settings.bonuses = Object.assign({}, defaults.bonuses, options ? options.bonuses : {} );
	settings.evaluators = pruneRules(Object.assign({}, defaults.evaluators, options ? options.evaluators : {} ));
	settings.evalSettings = Object.assign({}, defaults.evalSettings, options ? options.evalSettings : {} );
	Object.assign($.fn.passEval.messages, options ? options.messages : {});


	
	let input = this;
	let pluginId = 'passEval-' + $.fn.passEval.instances++;
	console.log(settings);
	render();
//PRIVATE

	function getNumRules() {
		return Object.keys(settings.rules).length
	}
	function getNumBonuses() {
		return Object.keys(settings.bonuses).length;
	}
	function getNumTests() {
		return getNumRules() + getNumBonuses();
	}
	function getRequiredEvalWeight() {
		let total = 0;
		let evals = settings.evaluators;
		if (!evals) return 0;
		for (let i in evals) {
			total += evals[i].required ? evals[i].required : 0;
		}
		return total;		
	}
	function getTotalEvalWeight() {
		let total = 0;
		let evals = settings.evaluators;
		if (!evals) return 0;
		for (let i in evals) {
			total += evals[i].weight ? evals[i].weight : 1;
		}
		return total;
	}
	function getRequiredWeight() {
		return getNumRules() + getRequiredEvalWeight();
	}
	function getTotalWeight() {
		return getTotalEvalWeight() + getNumRules() + getNumBonuses();
	}

	/*
	 * Remove any unenforced rules from settings, e.g defaults set to 'false'
	 * to allow for accurate test counts
	 */
	function pruneRules(rules) {
		console.log(rules);
		for (let i in rules) {
			if (!rules[i]) delete rules[i];
		}
		return rules;
	}

	/*
	 * Wraps the selected DOM element with plugin UI, creates and styles the progress bar
	 * Starts listening for key events on selected element
	 */
	function render() {
		$(input).wrap(`<div id='${pluginId}' class='passEval'></div>`);

		let svg = `<svg id='${pluginId}-svg' class='passEval-svg'>`;
		let total = getTotalWeight();
		console.log(total);
		let width = ((100/total) - 1);
		let offset = width +1;
		let index = 0;
		let numReqs = getNumRules() + getRequiredEvalWeight();
		//special cases for only one or two requirements
		svg += `<rect class='strengthBar' style='fill:red;x:${index * offset}%' width="${width}%"/>`;
		for (index = 0; index < getTotalWeight(); index++) {
			svg += `<rect class='strengthBar' style='fill:red;x:${index * offset}%' width="${width}%"/>`;
		}
		svg += '</svg>';

		$('#' + pluginId).append(svg);
		$('#' + pluginId).append(`<span id='${pluginId}-strength' class='passEval-strength'></span>`);
		$('#' + pluginId).append(`<span id='${pluginId}-error' class='passEval-error'></span>`);
		$('#' + pluginId).on('keyup', function(e) {
			if ($(input).val().length) evaluate();
			console.log(pluginId);
		});		
	}

	function getColor(index, total) {
		if (total == 0) return `rgb(0,255,0)`;
		let red = 255 - ((index/total) * 255);
		return `rgb(${Math.floor(red)},255,0)`;
	}
	/*
	 * @param name : the name of the validator function
	 * @param type : type of test : 'rule' or 'bonus' 
	 */
	function testValidator(name, type) {
		let rule = type == 'rule' ? settings.rules[name] : settings.bonuses[name];
		let value = $(input).val();
		let validators = $.fn.passEval.validators;
		if (!validators[name]) {
			console.log("Error : Called a nonexistent validator!");
			return false;
		}
		//if associated value is a boolen, validator takes no additional args
		if (typeof(rule) == 'boolean') {
			if (rule)  return validators[name](value);
		} else {
			return validators[name](value, rule);
		}	
	}

	function testEvaluator(name) {
		settings.evaluators[name] = Object.assign(settings.evalSettings, settings.evaluators[name]);
		let eval = settings.evaluators[name];
		let evaluators = $.fn.passEval.evaluators;
		let value = $(input).val();
		if (!evaluators[name]) {
			console.log("Error : Called a nonexistent evaluator!");
			return 0;
		}
		return evaluators[name](value, eval);

	}

	/*
	 * Runs each validator function specified in rules
	 * @return : the number of tests passed
	 */
	function validate (){
		console.log("validating " + pluginId);
		let value = $(input).val();
		let rules = settings.rules;
		let evals = settings.evaluators;
		let validators = $.fn.passEval.validators;
		let messages = $.fn.passEval.messages;
		let id = `#${pluginId}-error`;

		let passed = 0;
		for (let i in evals) {
			let num = testEvaluator(i);
			passed += Math.max(0, Math.min(num, evals[i].required));
			if (num < evals[i].required)
				$(id).text((messages[i] ? messages[i] : messages['default']));
		}
		for (let i in rules) {
			//test password against each specified rule
			if (testValidator(i, 'rule')) {
				passed++;	
			} else {
				$(id).text((messages[i] ? messages[i] : messages['default']));
			}
		}

		if (passed >= getRequiredWeight())
			$(id).text("");
		return passed;
	}

	/*
	 * Validates the required tests in rules
	 * If all rules pass, evaluates the bonus rules
	 * Updates the progress bar to indicate number of tests and bonuses passed
	 */
	function evaluate(){

		//validate mandatory tests
		let testsPassed = validate();
		let evals = settings.evaluators;
		console.log ( testsPassed + "/" + (getRequiredWeight()) +" rules followed." );
		//if all required tests pass, evaluate bonuses
		if (testsPassed >= getRequiredWeight()) {
			console.log("tests passed : " + testsPassed);
			let bonuses = settings.bonuses;
			for (let i in bonuses) {
				if (testValidator(i, 'bonus')) {
					testsPassed++;
				} 
			}
			for (let i in evals) {
				let num = testEvaluator(i);
				testsPassed += Math.max(0, Math.min(evals[i].weight, num) - evals[i].required);
			}

			$('#' + pluginId ).find(".strengthBar")
				.removeClass("currentStrength")
				.slice(0, testsPassed + 1).addClass("currentStrength")
				.css("fill", getColor(testsPassed - getRequiredWeight() , getTotalWeight() - getRequiredWeight()));		

		} else {
			$('#' + pluginId).find(".strengthBar")
				.removeClass('currentStrength')
				.slice(0,testsPassed + 1)
				.addClass("currentStrength").css("fill", "red");	
		}
	}
};

/*
 * Setup function, invokes self on load
 */
(function() {
	$.fn.passEval.instances = 0;
	let defaults = {
		rules : {
			requireSpecial : true,
			requireUpper : true,
			requireLower : true,
			requireNumeric : true,
			//requireMinLength : 6
		},
		granularity : 3
	};
	let settings = Object.assign({}, defaults);


	//validators return true or false depending on whether the test is passed
	$.fn.passEval.validators = {
		requireSpecial : function(value) {
    		return /[~`@!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(value);
		},
		requireUpper : function(value) {
	    	return /[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/g.test(value);
		},
		requireLower : function(value) {
			return /[abcdefghijklmnopqrstuvwxyz]/g.test(value);
		},
		requireNumeric : function(value) {
   			return /[0123456789]/g.test(value);
		},
		requireMinLength : function(value, min) {
			return value.length >= min;
		},
		requireMaxLength : function(value, max) {
			return value.length <= max;
		},		
	};

	$.fn.passEval.messages  = {
		requireSpecial : "Password must contain at least one special character.",
		requireUpper : "Password must contain at least one uppercase character.",
		requireLower : "Password must contain at least one lowercase character.",
		requireNumeric : "Password must contain at least one numeric character.",
		requireMinLength : `Password must contain at least ${settings.rules.requireMinLength} characters.`,
		requireMaxLength : `Password cannot contain more than ${settings.rules.requireMaxLength} characters.`,
		evaluateLength : `Password too short.`,
		default : "WARNING : No error message defined for this rule.",
	};

	//evaluators return a value, and add password strength up to the weight value
	$.fn.passEval.evaluators = {
		evaluateLength : function(value, settings) {
				if (value.length < settings.min) return 0;
				let range = settings.max - settings.min;
				let extra = value.length - settings.min;
				let percentFulfilled = extra / range;
				return 1 + Math.min(Math.floor(percentFulfilled * settings.weight), settings.weight);
		},
	};


})()